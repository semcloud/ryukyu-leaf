<div class="l-keyvisual">
  <div class="l-keyvisual__text">
    <h1><img src="<?php echo get_template_directory_uri(); ?>/images/home-logo.png" alt="アフターコロナ応援グルメサイト　沖縄GO!HAN旅"></h1>
    <p class="l-keyvisual__read">沖縄GO!HAN旅は<br class="br-sp" />美味しいグルメをたっぷり楽しみたい方と
      <br>コロナ対策をしながら<br class="br-sp" />頑張っているお店を応援します！
    </p>
    <p class="l-keyvisual__read">グルメ好きなあなたと飲食店をつなぐ<br>沖縄飲食店応援プロジェクト！</p>
    <div class="l-keyvisual__text-btnbox">
      <p><a href="<?php echo esc_url(home_url('shops')); ?>" class="btn btn-m-red"><i class="fas fa-list-ul"></i> 飲食店一覧</a></p>
      <p><a href="<?php echo esc_url(home_url('entry')); ?>" class="btn btn-m-white"><i class="fas fa-edit"></i> 新規店舗登録はこちら</a></p>
    </div>
  </div>
</div>
