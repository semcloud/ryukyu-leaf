<div class="owner">
  <div class="owner__inner">
    <h2 class="owner__title">飲食店の皆様へ</h2>
    <p class="owner__read">沖縄GO!HAN旅は、コロナ対策をしながら営業を再開した飲食店を応援したいと思い、このサイトを立ち上げました！<br>
      こんな時だからこそ、お客様にゆっくりグルメを楽しんでもらえる環境があるという事を知ってもらい、更にお店のファンが増えたらいいなと思っています。<br>
      無料で掲載できますので、ご賛同いただけましたら以下のフォームよりご連絡ください。</p>
    <p class="btn-wrap"><a href="<?php echo esc_url(home_url('entry-form')); ?>" class="btn btn-m-white"><i class="fas fa-edit"></i> 新規店舗登録はこちら</a></p>
  </div>
</div>
