      </main>
      <!-- <div id="backToTop">
        <a href="#header"><span class="text">一番上へ</span></a>
      </div> -->
      <footer class="l-footer">
        <div class="l-footer__bottom">
          <p class="copyright">&copy;琉球LEAF</p>
        </div>
      </footer>
    </div>

    <!-- Java script-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/vendors/slick.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/scripts/libs/top.js?<?php echo date('Ymd-Hi'); ?>"></script>
  </body>
</html>
